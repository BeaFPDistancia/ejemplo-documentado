# ejemplo Documentado

*calculoPrima
*doc


## Name

*Ejemplo para el cálculo de las primas de los empleados documentado*

## Description

El programa realiza el cálculo de las primas correspondientes, mediante la introducción por teclado de los datos del empleado, número de empleado, nombre, meses trabajados y si es o no Directivo.
En este caso se trata de poner de manifiesto la utilización de Javadoc para la documentación del código.

## Installation

Se considera que es un paquete de un progama, por lo que los requisitos deberán ser los del programa
base, y necesitaría acceso a la base de datos.

## Usage

El usuario introduce los datos del empleado y obtiene el resultado en pantalla. 
Según el tipo de acceso que tenga el usuario y los permisos, será solo una consulta o 
podrá realizar más operaciones (admin, programadores).

## Authors and acknowledgment

Documentación realizada por:

**Beatriz Menéndez Antuña**
Ingeniera de Minas cursando los ciclos formativos de grado superior de Desarrollo de Aplicaciones Multiplataforma y Desarrollo de Aplicaciones Web.

### Referencias utilizadas

Materiales de FP del MEFP (Ministerio de Educación y Formación Profesional) https://fpdistancia.educastur.es/

img/cropped-logo-CIFP-en-dos-lineas-1.jpg

