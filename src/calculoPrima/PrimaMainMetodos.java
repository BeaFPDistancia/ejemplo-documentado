/**
 *
 * 
 */
package calculoPrima;

import java.util.Scanner;

/**
 * <h2>Clase PrimaMainMetodos.</h2>
 * <h3>Clase con los m&eacute;todos correspondientes para que introduciendo por teclado los datos del empleado,
 *   n&uacute;mero de empleado, nombre del empleado, meses de trabajo y si es Directivo,
 *   realizar el c&aacute;lculo de la prima que le corresponde y mostrarlo en pantalla</h3>
 * 
 * 
 * @author Beatriz
 * @version 1.0
 * 
 */
public class PrimaMainMetodos {
	
	static Scanner teclado=new Scanner(System.in);
	
	/**
	 * M&eacute;todo main. C&aacute;lculo de las primas. Pedida de datos del empleado y resultado en pantalla
	 *   	
	 */
	
	public static void main(String[] args) {				
		
		int numEmple;
		String nomEmple;
		int meses;
		char esDirectivo;
		char respuesta;
		

		do {
			System.out.println("\nDATOS  EMPLEADO/A");
			numEmple=leerNumEmple();
			nomEmple=leerNomEmple();
			meses=leerMeses();
			esDirectivo=leerEsDirectivo();
			System.out.println("\n\tLe corresponde la prima "+hallarPrima(esDirectivo, meses));
			
			
			System.out.println("\n¿CALCULAR MAS PRIMAS? (S/N): ");
			respuesta=teclado.nextLine().toUpperCase().charAt(0);
		}while(respuesta=='S');		
	}
	
	/**
	 * M&eacute;todo que obtiene la prima correspondiente entre cuatro posibles seg&uacute;n los par&aacute;metros introducidos:
	 *  si es Directivo y si los meses de trabajo superan el a&ntilde;o (<i>P1</i>), si no lo es <i>P2</i>, 
	 *  si lo es pero no superan el a&ntilde;o (<i>P3</i>) y si no <i>P4</i>.
	 *  
	 * @param esDirectivo char que tomar&aacute; valor '+' si lo es y '-' si no
	 * @param meses n&uacute;mero de meses trabajados, puede ser inferior o superior al a&ntilde;o
	 * @return "P1", "P2", "P3" o "P4" el String que corresponda a la prima obtenida  
	 * 
	 */
	
	public static String hallarPrima(char esDirectivo, int meses) {
		if(esDirectivo=='+') // ES DIRECTIVO
			if(meses>=12)
				return "P1";
			else
				return "P3";
		else 	// NO ES DIRECTIVO
			if(meses>=12)
				return "P2";
			else
				return "P4";
	}

	/**
	 * M&eacute;todo que pide el n&uacute;mero del empleado, que será un n&uacute;mero que estar&aacute; comprendido entre 100 y 999 incluidos
	 *  y lo guarda en la variable <i>numEmple</i>
	 *  
	 * @return <i>numEmple</i> n&uacute;mero del empleado
	 * 
	 */
	
	public static int leerNumEmple() {		
		int numEmple;
		do{
			System.out.println("NÚMERO [100-999]: ");
			numEmple = teclado.nextInt();
		}while(numEmple<100 || numEmple>999);		
		teclado.nextLine();
		return numEmple;
	}
	
	/**
	 * M&eacute;todo que pide el nombre del empleado, el nombre no puede tener una longitud superior a 10 caracteres,
	 *  y lo guarda en la variable <i>nomEmple</i> 
	 * 
	 * @return <i>nomEmple</i> nombre del empleado
	 */
	
	
	public static String leerNomEmple() {
		String nomEmple;
		do {
			System.out.println("NOMBRE (max 10 caracteres): ");
			nomEmple = teclado.nextLine();
		}while(nomEmple.length()>10);		
		return nomEmple;
	}
	
	/**
	 * M&eacute;todo que pide el n&uacute;mero de meses de trabajo y lo guarda en la variable <i>meses</i>
	 * 
	 * @return <i>meses</i> n&uacute;mero de meses trabajados
	 */

	public static int leerMeses() {
		int meses;
		do {
			System.out.println("MESES DE TRABAJO: ");
			meses = teclado.nextInt();
		}while(meses<0);
		teclado.nextLine();
		return meses;
	}
	
	/**
	 * M&eacute;todo que pide si es Directivo guardar en una variable char llamada <i>esDirectivo</i>
	 *  que tomar&aacute; valores '+' (es directivo) o '-' (no lo es)
	 * 
	 * @return <i>esDirectivo</i> 
	 */

	public static char leerEsDirectivo() {
		char esDirectivo;
		do {
			System.out.println("¿ES DIRECTIVO? (+/-): ");
			esDirectivo = teclado.nextLine().charAt(0);
		}while(esDirectivo!='+' && esDirectivo!='-');
		return esDirectivo;
	}
	

}